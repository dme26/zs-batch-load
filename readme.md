# Example semi-automation on MacOS of Zeroshell static DHCP entry loading

- Apologies: I was originally just doing this to fix a problem quickly, so haven't generalised it across other platforms.
- It is likely that a framework such as [Selenium](http://www.seleniumhq.org) would provide a much more robust solution.

## One-time setup for bulk loading

- Start Safari.
- Log into your Zeroshell installation.
- Choose DHCP on the left menu.
- In a terminal window, run `gen-dhcp-js.sh` which will read line by line the bulk-loading source data from `host-ip-mac.tsv`
- Press enter to load the first JavaScript entry into the MacOS clipboard.

## Per-item bulk loading

- Switch back to Safari.
- Select "Add" above "Static IP Entries" which will present the pop-up window that we wish to populate and submit.
- Select Develop -> Show Web Inspector (or better, use the accelerator key) with the pop-up window focused.
- Paste and press enter: this will populate the field values, and submit the form if it validates.
- Switch back to your shell and press enter to load the next JavaScript chunk into the clipboard.
- Repeat until the shell script finishes.

# Notes

- Further automation is fairly straightforward, e.g. the "Add" button in the above steps can be "clicked" automatically through the JavaScript console also.
