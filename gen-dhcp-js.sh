#!/bin/bash

# Example script for semi-automating loading of DHCP configuration into Zeroshell
# Hacked together by David Eyers in the expectation of it being used once... if that...

# copy stdin so we check when user wants to continue
exec 7<&0
# read example bulk-loading data
exec < host-ip-mac.tsv

# read data record by record
while read NAME IP MAC
do
    read -u7 -p "Press enter to load clipboard with record for $NAME: "
    cat <<EOF | pbcopy
var n="$NAME"; var ip="$IP"; var mac="$MAC"; document.getElementsByName("Desc")[0].value = n; document.getElementsByName("IP")[0].value = ip; document.getElementsByName("MAC")[0].value = mac; if(validateData()) SubmitData();
EOF
done
